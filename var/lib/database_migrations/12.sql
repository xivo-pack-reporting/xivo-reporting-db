SET ROLE asterisk;

DROP FUNCTION IF EXISTS insert_stat_queue_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_queue_specific(start_date VARCHAR) RETURNS void AS
$$
INSERT INTO stat_queue_specific("time", queue_id, dst_num, nb_offered, answer_less_15, answer_btw_15_20, nb_abandoned, sum_resp_delay, abandoned_btw_15_20, abandoned_more_20, communication_time, wrapup_time) (
    SELECT thetime, queue_id, dst_num, t1.nb_offered, t1.answer_less_15, t1.answer_btw_15_20, t1.nb_abandoned, t1.sum_resp_delay, t1.abandoned_btw_15_20, t1.abandoned_more_20, t1.communication_time, t2.wrapup_time FROM 
        (SELECT round_to_15_minutes(c.end_time) AS thetime, s.queue_id, c.dst_num,
                sum(CASE WHEN s.status IN ('answered', 'abandoned', 'leaveempty') THEN 1 ELSE 0 END) AS nb_offered,
                sum(CASE WHEN s.status = 'answered' AND s.waittime <= 15 THEN 1 ELSE 0 END) AS answer_less_15,
                sum(CASE WHEN s.status = 'answered' AND s.waittime <= 20 AND s.waittime > 15 THEN 1 ELSE 0 END) AS answer_btw_15_20,
                sum(CASE WHEN s.status = 'abandoned' THEN 1 ELSE 0 END) AS nb_abandoned,
                sum(CASE WHEN s.status = 'answered' THEN s.waittime ELSE 0 END) AS sum_resp_delay,
                sum(CASE WHEN s.status = 'abandoned' AND s.waittime > 15 AND s.waittime <= 20 THEN 1 ELSE 0 END) AS abandoned_btw_15_20,
                sum(CASE WHEN s.status = 'abandoned' AND s.waittime > 20 THEN 1 ELSE 0 END) AS abandoned_more_20,
                sum(s.talktime) AS communication_time
            FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
            WHERE c.end_time > CAST($1 AS timestamp)
            GROUP BY thetime, s.queue_id, c.dst_num) t1 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, c.dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid
            WHERE q.event = 'WRAPUPSTART' AND c.end_time > CAST($1 AS timestamp)
            GROUP BY thetime, c.dst_num) t2
    )
$$
LANGUAGE sql;

DROP FUNCTION IF EXISTS insert_stat_agent_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_specific(start_date VARCHAR) RETURNS void AS
$$
INSERT INTO stat_agent_specific ("time", agent_id, nb_offered, nb_answered, conversation_time, ringing_time, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_received_internal_calls, nb_transfered_intern, nb_transfered_extern, nb_emitted_internal_calls, conversation_time_emitted_internal_calls, nb_incoming_calls, conversation_time_incoming_calls) (
    SELECT thetime, agent_id, t1.nb_offered, t2.nb_answered, t2.conversation_time, t3.total_ring_time, t4.nb_outgoing_calls, t4.conversation_time_outgoing_calls, t5.nb_received_internal_calls, t5.conversation_time_received_internal_calls, t2.nb_transfered_intern, t2.nb_transfered_extern, t4.nb_emitted_internal_calls, t4.conversation_time_emitted_internal_calls, t6.nb_incoming_calls, t6.conversation_time_incoming_calls FROM
        (SELECT round_to_15_minutes(c.end_time) AS thetime, s.id AS agent_id, count(*) AS nb_offered
            FROM queue_log q INNER JOIN stat_agent s ON q.agent = s.name
            INNER JOIN call_data c ON c.uniqueid = q.callid
            WHERE event IN('CONNECT', 'RINGNOANSWER') AND c.end_time > CAST($1 AS timestamp)
            GROUP BY thetime, agent_id) t1 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, agent_id,
                count(*) AS nb_answered,
                sum(talktime) AS conversation_time,
                sum(CASE WHEN c.transfered IS TRUE AND c.transfer_direction = 'internal' THEN 1 ELSE 0 END) AS nb_transfered_intern,
                sum(CASE WHEN c.transfered IS TRUE AND c.transfer_direction = 'outgoing' THEN 1 ELSE 0 END) AS nb_transfered_extern
            FROM stat_call_on_queue s JOIN call_data c ON s.callid = c.uniqueid
            WHERE s.status = 'answered' AND c.end_time > CAST($1 AS timestamp)
            GROUP BY thetime, agent_id) t2 NATURAL FULL JOIN
        (SELECT thetime, agent_id, total_ring_time FROM ringing_time($1)) t3 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_id,
                sum(CASE WHEN c.call_direction = 'outgoing' THEN 1 ELSE 0 END) AS nb_outgoing_calls,
                floor(EXTRACT(epoch FROM sum(CASE WHEN c.call_direction = 'outgoing' THEN c.end_time - c.answer_time ELSE INTERVAL '0' END))) AS conversation_time_outgoing_calls,
                sum(CASE WHEN c.call_direction = 'internal' THEN 1 ELSE 0 END) AS nb_emitted_internal_calls,
                floor(EXTRACT(epoch FROM sum(CASE WHEN c.call_direction = 'internal' THEN c.end_time - c.answer_time ELSE INTERVAL '0' END))) AS conversation_time_emitted_internal_calls
            FROM call_data c INNER JOIN agent_position a ON a.line_number = c.src_num
            WHERE c.end_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
            GROUP BY thetime, agent_id) t4 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_id, count(*) AS nb_received_internal_calls, floor(EXTRACT(epoch FROM sum(c.end_time - c.answer_time))) AS conversation_time_received_internal_calls
            FROM call_data c INNER JOIN agent_position a ON a.line_number = c.dst_num
            WHERE c.call_direction = 'internal' AND c.end_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time <= a.end_time OR a.end_time IS NULL)
            GROUP BY thetime, agent_id) t5 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_id, count(*) AS nb_incoming_calls, floor(EXTRACT(epoch FROM sum(c.end_time - c.answer_time))) AS conversation_time_incoming_calls
            FROM call_data c INNER JOIN agent_position a ON a.sda = c.dst_num
            WHERE c.end_time > CAST($1 AS TIMESTAMP) AND c.call_direction = 'incoming'
            AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
            GROUP BY thetime, agent_id) t6
)
$$
LANGUAGE sql;

DROP FUNCTION IF EXISTS ringing_time(varchar);
CREATE OR REPLACE FUNCTION ringing_time(start_date varchar) RETURNS TABLE(thetime timestamp without time zone, agent_id integer, total_ring_time integer)
AS
$$
SELECT rtime, ragent_id, (CASE WHEN ring_time_answered IS NULL THEN ring_time_ignored
                               WHEN ring_time_ignored IS NULL THEN ring_time_answered
                               ELSE ring_time_answered + ring_time_ignored
                          END) AS total_ring_time FROM
    (SELECT round_to_15_minutes(call.end_time) AS rtime, stat.agent_id AS ragent_id, CAST(sum(call.ring_duration_on_answer) AS INTEGER) AS ring_time_answered
        FROM stat_call_on_queue stat JOIN call_data call ON stat.callid = call.uniqueid
        WHERE stat.status = 'answered' AND call.end_time > CAST($1 AS timestamp)
        GROUP BY rtime, ragent_id) t1 NATURAL FULL JOIN
    (SELECT round_to_15_minutes(c.end_time) AS rtime, s.id AS ragent_id, CAST(sum(CAST(q.data1 AS integer)/1000) AS INTEGER) AS ring_time_ignored
        FROM queue_log q INNER JOIN stat_agent s ON q.agent = s.name
        INNER JOIN call_data c ON c.uniqueid = q.callid
        WHERE event = 'RINGNOANSWER' AND c.end_time > CAST($1 AS timestamp) 
        GROUP BY rtime, ragent_id) t2;
$$
LANGUAGE sql;


DROP FUNCTION IF EXISTS insert_stat_agent_queue_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_queue_specific(start_date VARCHAR) RETURNS void AS
$$
INSERT INTO stat_agent_queue_specific ("time", agent_id, queue_id, dst_num, nb_answered_calls, communication_time, wrapup_time) (
    SELECT t1.thetime, t1.agent_id, t1.queue_id, t1.dst_num, t1.nb_answered_calls, t1.communication_time, t2.wrapup_time FROM
        (SELECT round_to_15_minutes(c.end_time) AS thetime, s.agent_id AS agent_id, s.queue_id AS queue_id, c.dst_num AS dst_num, count(*) AS nb_answered_calls, sum(s.talktime) AS communication_time
            FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid WHERE s.status = 'answered' AND c.end_time >= CAST($1 AS timestamp)
            GROUP BY thetime, agent_id, queue_id, dst_num) t1 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, s.agent_id AS agent_id, c.dst_num AS dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid JOIN stat_call_on_queue s ON s.callid = q.callid
            WHERE q.event = 'WRAPUPSTART' AND c.end_time >= CAST($1 AS timestamp)
            GROUP BY thetime, agent_id, dst_num) t2
);
$$
LANGUAGE sql;

