SET ROLE asterisk;

CREATE OR REPLACE FUNCTION round_to_x_seconds(the_date timestamp without time zone, seconds integer) RETURNS timestamp without time zone AS
$$
DECLARE
  delta INTERVAL;
  new_date TIMESTAMP;
BEGIN
  delta := the_date - the_date::timestamp with time zone at time zone 'UTC';
  new_date = the_date::timestamp(0);

  RETURN new_date - (((CAST(extract(epoch FROM new_date + delta) AS INTEGER) + 3*24*3600) % seconds) * INTERVAL '1 second');
END
$$
LANGUAGE plpgsql;
