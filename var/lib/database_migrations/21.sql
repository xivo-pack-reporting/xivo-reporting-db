SET ROLE asterisk;

CREATE TABLE hold_periods (
    id SERIAL PRIMARY KEY,
    uniqueid VARCHAR(150),
    linkedid VARCHAR(150),
    start TIMESTAMP WITHOUT TIME ZONE,
    "end" TIMESTAMP WITHOUT TIME ZONE
);

GRANT SELECT ON hold_periods TO stats;

