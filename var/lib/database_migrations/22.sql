SET ROLE asterisk;

CREATE OR REPLACE FUNCTION insert_stat_queue_specific(start_date VARCHAR, end_date VARCHAR) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_queue_specific("time", queue_ref, dst_num, nb_offered, answer_less_15, answer_btw_15_20, nb_abandoned, sum_resp_delay, abandoned_btw_15_20, abandoned_more_20, communication_time, wrapup_time, hold_time) (
    SELECT thetime, queue_ref, dst_num, t1.nb_offered, t1.answer_less_15, t1.answer_btw_15_20, t1.nb_abandoned, t1.sum_resp_delay, t1.abandoned_btw_15_20, t1.abandoned_more_20, EXTRACT(epoch FROM t3.communication_time), t2.wrapup_time, EXTRACT(epoch FROM t4.hold_time) FROM 
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num,
                sum(CASE WHEN cq.status IN (''answered'', ''abandoned'', ''leaveempty'') THEN 1 ELSE 0 END) AS nb_offered,
                sum(CASE WHEN cq.status = ''answered'' AND EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) <= 15 THEN 1 ELSE 0 END) AS answer_less_15,
                sum(CASE WHEN cq.status = ''answered'' AND EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) <= 20 AND EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) > 15 THEN 1 ELSE 0 END) AS answer_btw_15_20,
                sum(CASE WHEN cq.status = ''abandoned'' THEN 1 ELSE 0 END) AS nb_abandoned,
                sum(CASE WHEN cq.status = ''answered'' THEN EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) ELSE 0 END) AS sum_resp_delay,
                sum(CASE WHEN cq.status = ''abandoned'' AND EXTRACT(epoch FROM (cq.hangup_time - cq.queue_time)) > 15 AND EXTRACT(epoch FROM (cq.hangup_time - cq.queue_time)) <= 20 THEN 1 ELSE 0 END) AS abandoned_btw_15_20,
                sum(CASE WHEN cq.status = ''abandoned'' AND EXTRACT(epoch FROM (cq.hangup_time - cq.queue_time)) > 20 THEN 1 ELSE 0 END) AS abandoned_more_20
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.queue_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t1 NATURAL FULL JOIN
        (SELECT $1::timestamp AS thetime, q.queuename AS queue_ref, c.dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid
            WHERE q.event = ''WRAPUPSTART'' AND q.time >= $1 AND q.time < $2
            GROUP BY thetime, queue_ref, c.dst_num) t2 NATURAL FULL JOIN
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num, sum(least($2::timestamp, cq.hangup_time) - greatest($1::timestamp, cq.answer_time)) AS communication_time
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t3 NATURAL FULL JOIN
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num, sum(least($2::timestamp, hp.end) - greatest($1::timestamp, hp.start)) AS hold_time
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid INNER JOIN hold_periods hp ON hp.linkedid = c.uniqueid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL) AND hp.start < $2::timestamp AND (hp.end IS NULL OR hp.end >= $1::timestamp)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t4
    )';
    EXECUTE query USING start_date, end_date;
END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insert_stat_agent_specific(start_date VARCHAR, end_date VARCHAR) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_agent_specific ("time", agent_num, nb_offered, nb_answered, conversation_time, ringing_time, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_received_internal_calls, nb_transfered_intern, nb_transfered_extern, nb_emitted_internal_calls, conversation_time_emitted_internal_calls, nb_incoming_calls, conversation_time_incoming_calls, hold_time) (
    SELECT $1::timestamp, agent_num, t1.nb_offered, t1.nb_answered, EXTRACT(epoch FROM t2.conversation_time), t3.total_ring_time, t4.nb_outgoing_calls, EXTRACT(epoch FROM t7.conversation_time_outgoing_calls), t5.nb_received_internal_calls, EXTRACT(epoch FROM t8.conversation_time_received_internal_calls), t6.nb_transfered_intern, t6.nb_transfered_extern, t4.nb_emitted_internal_calls, EXTRACT(epoch FROM t7.conversation_time_emitted_internal_calls), t5.nb_incoming_calls, EXTRACT(epoch FROM t8.conversation_time_incoming_calls), EXTRACT(epoch FROM t9.hold_time) FROM
        (SELECT regexp_replace(agent, ''Agent/'', '''') AS agent_num, count(*) AS nb_offered, sum(CASE WHEN event = ''CONNECT'' THEN 1 ELSE 0 END) AS nb_answered
            FROM queue_log WHERE event IN(''CONNECT'', ''RINGNOANSWER'') AND time >= $1 AND time < $2
            GROUP BY agent_num) t1 NATURAL FULL JOIN
        (SELECT cq.agent_num, sum(least($2::timestamp, cq.hangup_time) - greatest($1::timestamp, cq.answer_time)) AS conversation_time
            FROM call_on_queue cq WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY agent_num) t2 NATURAL FULL JOIN
        (SELECT agent_num, total_ring_time FROM ringing_time($1, $2)) t3 NATURAL FULL JOIN
        (SELECT a.agent_num, sum(CASE WHEN c.call_direction = ''outgoing'' THEN 1 ELSE 0 END) AS nb_outgoing_calls,
                             sum(CASE WHEN c.call_direction = ''internal'' THEN 1 ELSE 0 END) AS nb_emitted_internal_calls
            FROM call_data c INNER JOIN agent_position a ON a.line_number = c.src_num
            WHERE c.start_time < $2::timestamp AND (c.end_time >= $1::timestamp OR c.end_time IS NULL) AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
            GROUP BY agent_num) t4 NATURAL FULL JOIN
        (SELECT a.agent_num, sum(CASE WHEN c.call_direction = ''internal'' THEN 1 ELSE 0 END) AS nb_received_internal_calls,
                             sum(CASE WHEN c.call_direction = ''incoming'' THEN 1 ELSE 0 END) AS nb_incoming_calls
            FROM call_data c INNER JOIN agent_position a ON (a.line_number = c.dst_num OR a.sda = c.dst_num)
            WHERE c.start_time < $2::timestamp AND (c.end_time >= $1::timestamp OR c.end_time IS NULL) AND c.start_time >= a.start_time AND (c.start_time <= a.end_time OR a.end_time IS NULL)
            GROUP BY agent_num) t5 NATURAL FULL JOIN
        (SELECT regexp_replace(q.agent, ''Agent/'', '''') AS agent_num, sum(CASE WHEN c.transfer_direction = ''internal'' THEN 1 ELSE 0 END) AS nb_transfered_intern,
                                                                        sum(CASE WHEN c.transfer_direction = ''outgoing'' THEN 1 ELSE 0 END) AS nb_transfered_extern
            FROM queue_log q INNER JOIN call_data c ON q.callid = c.uniqueid
            WHERE q.event = ''TRANSFER'' AND q.time >= $1 AND q.time < $2
            GROUP BY agent_num) t6 NATURAL FULL JOIN
        (SELECT a.agent_num,
                sum(CASE WHEN c.call_direction = ''outgoing'' THEN least($2::timestamp, c.end_time) - greatest($1::timestamp, c.answer_time) ELSE INTERVAL ''0'' END) AS conversation_time_outgoing_calls,
                sum(CASE WHEN c.call_direction = ''internal'' THEN least($2::timestamp, c.end_time) - greatest($1::timestamp, c.answer_time) ELSE INTERVAL ''0'' END) AS conversation_time_emitted_internal_calls
            FROM call_data c INNER JOIN agent_position a ON a.line_number = c.src_num
            WHERE c.answer_time < $2::timestamp AND (c.end_time >= $1::timestamp OR c.end_time IS NULL) AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
            GROUP BY agent_num) t7 NATURAL FULL JOIN
        (SELECT a.agent_num,
                sum(CASE WHEN c.call_direction = ''internal'' THEN least($2::timestamp, c.end_time) - greatest($1::timestamp, c.answer_time) ELSE INTERVAL ''0'' END) AS conversation_time_received_internal_calls,
                sum(CASE WHEN c.call_direction = ''incoming'' THEN least($2::timestamp, c.end_time) - greatest($1::timestamp, c.answer_time) ELSE INTERVAL ''0'' END) AS conversation_time_incoming_calls
            FROM call_data c INNER JOIN agent_position a ON (a.line_number = c.dst_num OR a.sda = c.dst_num)
            WHERE c.answer_time < $2::timestamp AND (c.end_time >= $1::timestamp OR c.end_time IS NULL) AND c.start_time >= a.start_time AND (c.start_time <= a.end_time OR a.end_time IS NULL)
            GROUP BY agent_num) t8 NATURAL FULL JOIN
        (SELECT cq.agent_num, sum(least($2::timestamp, hp.end) - greatest($1::timestamp, hp.start)) AS hold_time
            FROM call_on_queue cq INNER JOIN hold_periods hp ON hp.linkedid = cq.callid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL) AND hp.start < $2::timestamp AND (hp.end IS NULL OR hp.end >= $1::timestamp)
            GROUP BY cq.agent_num) t9
)';
    EXECUTE query USING start_date, end_date;
END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insert_stat_agent_queue_specific(start_date VARCHAR, end_date varchar) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_agent_queue_specific ("time", agent_num, queue_ref, dst_num, nb_answered_calls, communication_time, wrapup_time, hold_time) (
    SELECT $1::timestamp, t1.agent_num, t1.queue_ref, t1.dst_num, t1.nb_answered_calls, EXTRACT(epoch FROM t1.communication_time), t2.wrapup_time, EXTRACT(epoch FROM t3.hold_time) FROM
        (SELECT cq.agent_num AS agent_num, cq.queue_ref AS queue_ref, c.dst_num AS dst_num,
                count(*) AS nb_answered_calls,
                sum(least($2::timestamp, cq.hangup_time) - greatest($1::timestamp, cq.answer_time)) AS communication_time
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.status = ''answered'' AND cq.queue_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY agent_num, queue_ref, dst_num) t1 NATURAL FULL JOIN
        (SELECT regexp_replace(q.agent, ''Agent/'', '''') AS agent_num, q.queuename AS queue_ref, c.dst_num AS dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid WHERE event = ''WRAPUPSTART'' AND time >= $1 AND time < $2
            GROUP BY agent_num, queue_ref, dst_num) t2 NATURAL FULL JOIN
        (SELECT cq.agent_num, cq.queue_ref, cd.dst_num, sum(least($2::timestamp, hp.end) - greatest($1::timestamp, hp.start)) AS hold_time
            FROM call_on_queue cq INNER JOIN call_data cd ON cq.callid = cd.uniqueid INNER JOIN hold_periods hp ON hp.linkedid = cq.callid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL) AND hp.start < $2::timestamp AND (hp.end IS NULL OR hp.end >= $1::timestamp)
            GROUP BY cq.agent_num, queue_ref, dst_num) t3
)';
    EXECUTE query USING start_date, end_date;
END
$$
LANGUAGE plpgsql;

CREATE INDEX hold_periods__idx__linkedid ON hold_periods(linkedid);
