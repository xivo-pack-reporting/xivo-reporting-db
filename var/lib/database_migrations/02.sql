SET ROLE asterisk;

CREATE TYPE call_direction_type AS ENUM('incoming', 'outgoing', 'internal');

ALTER TABLE call_data DROP COLUMN dst_context;
ALTER TABLE call_data ADD COLUMN call_direction call_direction_type;
ALTER TABLE call_data ADD COLUMN src_num VARCHAR(80);
