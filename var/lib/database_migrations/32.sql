SET ROLE asterisk;

CREATE OR REPLACE FUNCTION round_to_x_seconds(the_date timestamp without time zone, seconds integer) RETURNS timestamp without time zone AS
$$
DECLARE
  delta INTERVAL;
BEGIN
  delta := the_date - the_date::timestamp with time zone at time zone 'UTC';
  RETURN the_date - (((CAST(extract(epoch FROM the_date + delta) AS INTEGER) + 3*24*3600) % seconds) * INTERVAL '1 second');
END
$$
LANGUAGE plpgsql;

ALTER TABLE hold_periods ADD COLUMN uniqueid VARCHAR(150);
