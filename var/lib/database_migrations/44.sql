SET ROLE asterisk;

CREATE TABLE call_element (
    id SERIAL PRIMARY KEY,
    call_data_id INTEGER REFERENCES call_data(id) NOT NULL,
    start_time TIMESTAMP WITHOUT TIME ZONE,
    answer_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    interface VARCHAR(255),
    agent VARCHAR(128)
);
CREATE INDEX call_element__idx__call_data_id ON call_element(call_data_id);
CREATE INDEX call_element__idx__interface ON call_element(interface);
