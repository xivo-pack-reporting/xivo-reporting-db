SET ROLE asterisk;

CREATE TEMPORARY TABLE copy_stat_agent_periodic AS
    SELECT time, regexp_replace(sa.name, 'Agent/', '') AS agent, login_time, pause_time, wrapup_time
    FROM stat_agent_periodic sap JOIN stat_agent sa ON sap.agent_id = sa.id;

DROP TYPE IF EXISTS agent_state_type CASCADE;
CREATE TYPE agent_state_type AS ENUM('logged_on', 'logged_off', 'paused');

DROP TABLE IF EXISTS agent_states;
CREATE TABLE agent_states (
    agent VARCHAR(128),
    time TIMESTAMP WITHOUT TIME ZONE,
    state agent_state_type
);

DROP TABLE IF EXISTS "stat_agent_periodic" CASCADE;
CREATE TABLE "stat_agent_periodic" (
 "id" SERIAL PRIMARY KEY,
 "time" timestamp NOT NULL,
 "agent" VARCHAR(128),
 "login_time" INTERVAL NOT NULL DEFAULT '00:00:00',
 "pause_time" INTERVAL NOT NULL DEFAULT '00:00:00',
 "wrapup_time" INTERVAL NOT NULL DEFAULT '00:00:00'
);

GRANT SELECT ON ALL TABLES IN SCHEMA public TO stats;

INSERT INTO stat_agent_periodic(time, agent, login_time, pause_time, wrapup_time) (
    SELECT time, agent, login_time, pause_time, wrapup_time FROM copy_stat_agent_periodic);
