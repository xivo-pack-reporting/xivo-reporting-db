SET ROLE asterisk;

ALTER TABLE stat_agent_specific RENAME conversation_time_internal_calls TO conversation_time_received_internal_calls;

DROP FUNCTION IF EXISTS insert_stat_agent_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'INSERT INTO stat_agent_specific ("time", agent_id, nb_offered, nb_answered, conversation_time, ringing_time, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_received_internal_calls, nb_transfered_intern, nb_transfered_extern) (
        SELECT thetime, agent_id, t1.nb_offered, t2.nb_answered, t3.conversation_time, t4.total_ring_time, t5.nb_outgoing_calls, t6.conversation_time_outgoing_calls, t7.nb_received_internal_calls, t8.conversation_time_received_internal_calls, t9.nb_transfered_intern, t10.nb_transfered_extern FROM
            (SELECT round_to_15_minutes(CAST(q.time AS  timestamp)) AS thetime, s.id AS agent_id, count(*) AS nb_offered
                FROM queue_log q INNER JOIN stat_agent s ON q.agent = s.name
                WHERE event IN(''CONNECT'', ''RINGNOANSWER'') AND CAST(time AS timestamp) > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t1 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(CAST(q.time AS  timestamp)) AS thetime, s.id AS agent_id, count(*) AS nb_answered
                FROM queue_log q INNER JOIN stat_agent s ON q.agent = s.name
                WHERE event = ''CONNECT'' AND CAST(time AS timestamp) > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t2 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(CAST(time AS  timestamp)) AS thetime, agent_id, sum(talktime) AS conversation_time
                FROM stat_call_on_queue WHERE status = ''answered'' AND time > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t3 NATURAL FULL JOIN
            (SELECT thetime, agent_id, total_ring_time FROM ringing_time($1)) t4 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.start_time) AS thetime, a.agent_id, count(*) AS nb_outgoing_calls
                FROM call_data c INNER JOIN agent_position a ON a.line_number = c.src_num
                WHERE c.start_time > CAST($1 AS TIMESTAMP) AND c.call_direction = ''outgoing''
                AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t5 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.start_time) AS thetime, a.agent_id, floor(EXTRACT(epoch FROM sum(c.end_time - c.answer_time))) AS conversation_time_outgoing_calls
                FROM call_data c INNER JOIN agent_position a ON a.line_number = c.src_num
                WHERE c.start_time > CAST($1 AS TIMESTAMP) AND c.call_direction = ''outgoing''
                AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t6 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_id, count(*) AS nb_received_internal_calls
                FROM call_data c INNER JOIN agent_position a ON a.line_number = c.dst_num
                WHERE c.call_direction = ''internal'' AND c.end_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time <= a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t7 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_id, floor(EXTRACT(epoch FROM sum(c.end_time - c.answer_time))) AS conversation_time_received_internal_calls
                FROM call_data c INNER JOIN agent_position a ON a.line_number = c.dst_num
                WHERE  c.call_direction = ''internal'' AND c.end_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time <= a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t8 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(CAST(s.time AS  timestamp)) AS thetime, s.agent_id, count(*) AS nb_transfered_intern
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND c.transfered is true AND c.transfer_direction = ''internal'' AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t9 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(CAST(s.time AS  timestamp)) AS thetime, s.agent_id, count(*) AS nb_transfered_extern
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND c.transfered is true AND c.transfer_direction = ''outgoing'' AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t10
    )';
    EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS insert_stat_queue_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_queue_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'INSERT INTO stat_queue_specific("time", queue_id, dst_num, nb_offered, nb_abandoned, sum_resp_delay, answer_less_15, abandoned_btw_15_20, answer_btw_15_20, abandoned_more_20, communication_time) (
        SELECT t1.thetime, t1.queue_id, t1.dst_num, t1.nb_offered, t2.nb_abandoned, t3.sum_resp_delay, t4.answer_less_15, t5.abandoned_btw_15_20, t6.answer_btw_15_20, t7.abandoned_more_20, t8.communication_time FROM 
            (SELECT round_to_15_minutes(s.time) AS thetime, s.queue_id, c.dst_num, count(*) AS nb_offered
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status IN(''answered'', ''abandoned'') AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, s.queue_id, c.dst_num) t1 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS nb_abandoned
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''abandoned'' AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t2 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, sum(s.waittime) AS sum_resp_delay
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t3 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS answer_less_15
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND s.waittime <= 15 AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t4 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS abandoned_btw_15_20
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''abandoned'' AND s.waittime > 15 AND s.waittime <= 20 AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t5 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS answer_btw_15_20
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND s.waittime <= 20 AND s.waittime > 15 AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t6 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS abandoned_more_20
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''abandoned'' AND s.waittime > 20 AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t7 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, sum(talktime) AS communication_time
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t8 
        )';
    EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION create_columns_if_needed() RETURNS void AS
$$
DECLARE
    tmp VARCHAR;
BEGIN
    select column_name INTO tmp from information_schema.columns where table_name = 'stat_agent_specific' and column_name = 'nb_transfered_intern';
    IF tmp IS NULL
    THEN
        ALTER TABLE stat_agent_specific ADD COLUMN nb_transfered_intern INTEGER;
        ALTER TABLE stat_agent_specific ADD COLUMN nb_transfered_extern INTEGER;
        UPDATE stat_agent_specific SET nb_transfered_intern = nb_transfered;
        ALTER TABLE stat_agent_specific DROP COLUMN nb_transfered;
    END IF;
END;
$$
LANGUAGE plpgsql;

select 1 as place_holder from create_columns_if_needed();

