SET ROLE asterisk;

-- migration de stat_agent_specific
CREATE TEMPORARY TABLE copy_stat_agent_specific AS
    SELECT "time", regexp_replace(sa.name, 'Agent/', '') AS agent_num, nb_offered, nb_answered, conversation_time, ringing_time, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_received_internal_calls, nb_transfered_intern, nb_transfered_extern, nb_emitted_internal_calls, conversation_time_emitted_internal_calls, nb_incoming_calls, conversation_time_incoming_calls FROM stat_agent_specific sap JOIN stat_agent sa ON sap.agent_id = sa.id;

TRUNCATE stat_agent_specific;

ALTER TABLE stat_agent_specific DROP COLUMN agent_id;
ALTER TABLE stat_agent_specific ADD COLUMN agent_num VARCHAR(50);

INSERT INTO stat_agent_specific("time", agent_num, nb_offered, nb_answered, conversation_time, ringing_time, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_received_internal_calls, nb_transfered_intern, nb_transfered_extern, nb_emitted_internal_calls, conversation_time_emitted_internal_calls, nb_incoming_calls, conversation_time_incoming_calls) (
    SELECT "time", agent_num, nb_offered, nb_answered, conversation_time, ringing_time, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_received_internal_calls, nb_transfered_intern, nb_transfered_extern, nb_emitted_internal_calls, conversation_time_emitted_internal_calls, nb_incoming_calls, conversation_time_incoming_calls FROM copy_stat_agent_specific);

--migration de stat_queue_specific
CREATE TEMPORARY TABLE copy_stat_queue_specific AS
    SELECT "time", sq.name AS queue_ref, dst_num, nb_offered, answer_less_15, answer_btw_15_20, nb_abandoned, sum_resp_delay, abandoned_btw_15_20, abandoned_more_20, communication_time, wrapup_time FROM stat_queue_specific sqp JOIN stat_queue sq ON sqp.queue_id = sq.id;

TRUNCATE stat_queue_specific;

ALTER TABLE stat_queue_specific DROP COLUMN queue_id;
ALTER TABLE stat_queue_specific ADD COLUMN queue_ref VARCHAR(50);

INSERT INTO stat_queue_specific("time", queue_ref, dst_num, nb_offered, answer_less_15, answer_btw_15_20, nb_abandoned, sum_resp_delay, abandoned_btw_15_20, abandoned_more_20, communication_time, wrapup_time) (
    SELECT "time", queue_ref, dst_num, nb_offered, answer_less_15, answer_btw_15_20, nb_abandoned, sum_resp_delay, abandoned_btw_15_20, abandoned_more_20, communication_time, wrapup_time FROM copy_stat_queue_specific);

--migration stat_agent_queue_specific
CREATE TEMPORARY TABLE copy_stat_agent_queue_specific AS
    SELECT "time", regexp_replace(sa.name, 'Agent/', '') AS agent_num, sq.name AS queue_ref, dst_num, nb_answered_calls, communication_time, wrapup_time FROM stat_agent_queue_specific saqp JOIN stat_agent sa ON saqp.agent_id = sa.id JOIN stat_queue sq ON saqp.queue_id = sq.id;

TRUNCATE stat_agent_queue_specific;

ALTER TABLE stat_agent_queue_specific DROP COLUMN queue_id;
ALTER TABLE stat_agent_queue_specific DROP COLUMN agent_id;
ALTER TABLE stat_agent_queue_specific ADD COLUMN agent_num VARCHAR(50);
ALTER TABLE stat_agent_queue_specific ADD COLUMN queue_ref VARCHAR(50);

INSERT INTO stat_agent_queue_specific("time", agent_num, queue_ref, dst_num, nb_answered_calls, communication_time, wrapup_time) (
    SELECT "time", agent_num, queue_ref, dst_num, nb_answered_calls, communication_time, wrapup_time FROM copy_stat_agent_queue_specific);

--modif de la table agent_position
ALTER TABLE agent_position DROP COLUMN agent_id;
ALTER TABLE agent_position ADD COLUMN agent_num VARCHAR(50);

--suppression des tables devenues inutiles
DROP TABLE stat_agent_agentfeatures;
DROP TABLE stat_agent;
DROP TABLE stat_queue;

