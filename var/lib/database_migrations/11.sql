SET ROLE asterisk;

CREATE TABLE attached_data (
    id SERIAL PRIMARY KEY,
    id_call_data INTEGER REFERENCES call_data(id),
    key VARCHAR(128),
    value VARCHAR(256)
);

CREATE INDEX "stat_call_on_queue__idx__callid" ON stat_call_on_queue(callid);
CREATE INDEX "call_data__idx__start_time" ON call_data(start_time);
