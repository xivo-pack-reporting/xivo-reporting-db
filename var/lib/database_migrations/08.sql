SET ROLE asterisk;

DROP FUNCTION IF EXISTS insert_stat_agent_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'INSERT INTO stat_agent_specific ("time", agent_id, nb_offered, nb_answered, conversation_time, ringing_time, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_received_internal_calls, nb_transfered_intern, nb_transfered_extern, nb_emitted_internal_calls, conversation_time_emitted_internal_calls, nb_incoming_calls, conversation_time_incoming_calls) (
        SELECT thetime, agent_id, t1.nb_offered, t2.nb_answered, t2.conversation_time, t3.total_ring_time, t4.nb_outgoing_calls, t4.conversation_time_outgoing_calls, t5.nb_received_internal_calls, t5.conversation_time_received_internal_calls, t6.nb_transfered_intern, t6.nb_transfered_extern, t4.nb_emitted_internal_calls, t4.conversation_time_emitted_internal_calls, t7.nb_incoming_calls, t7.conversation_time_incoming_calls FROM
            (SELECT round_to_15_minutes(CAST(q.time AS  timestamp)) AS thetime, s.id AS agent_id, count(*) AS nb_offered
                FROM queue_log q INNER JOIN stat_agent s ON q.agent = s.name
                WHERE event IN(''CONNECT'', ''RINGNOANSWER'') AND CAST(time AS timestamp) > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t1 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(time) AS thetime, agent_id, count(*) AS nb_answered, sum(talktime) AS conversation_time
                FROM stat_call_on_queue WHERE status = ''answered'' AND time > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t2 NATURAL FULL JOIN
            (SELECT thetime, agent_id, total_ring_time FROM ringing_time($1)) t3 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.start_time) AS thetime, a.agent_id,
                    sum(CASE WHEN c.call_direction = ''outgoing'' THEN 1 ELSE 0 END) AS nb_outgoing_calls,
                    floor(EXTRACT(epoch FROM sum(CASE WHEN c.call_direction = ''outgoing'' THEN c.end_time - c.answer_time ELSE INTERVAL ''0'' END))) AS conversation_time_outgoing_calls,
                    sum(CASE WHEN c.call_direction = ''internal'' THEN 1 ELSE 0 END) AS nb_emitted_internal_calls,
                    floor(EXTRACT(epoch FROM sum(CASE WHEN c.call_direction = ''internal'' THEN c.end_time - c.answer_time ELSE INTERVAL ''0'' END))) AS conversation_time_emitted_internal_calls
                FROM call_data c INNER JOIN agent_position a ON a.line_number = c.src_num
                WHERE c.start_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t4 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_id, count(*) AS nb_received_internal_calls, floor(EXTRACT(epoch FROM sum(c.end_time - c.answer_time))) AS conversation_time_received_internal_calls
                FROM call_data c INNER JOIN agent_position a ON a.line_number = c.dst_num
                WHERE c.call_direction = ''internal'' AND c.end_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time <= a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t5 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(CAST(s.time AS  timestamp)) AS thetime, s.agent_id, sum(CASE WHEN c.transfer_direction = ''internal'' THEN 1 ELSE 0 END) AS nb_transfered_intern, sum(CASE WHEN c.transfer_direction = ''outgoing'' THEN 1 ELSE 0 END) AS nb_transfered_extern
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND c.transfered is true AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t6 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.start_time) AS thetime, a.agent_id, count(*) AS nb_incoming_calls, floor(EXTRACT(epoch FROM sum(c.end_time - c.answer_time))) AS conversation_time_incoming_calls
                FROM call_data c INNER JOIN agent_position a ON a.sda = c.dst_num
                WHERE c.start_time > CAST($1 AS TIMESTAMP) AND c.call_direction = ''incoming''
                AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t7
    )';
    EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;
