SET ROLE asterisk;

DROP FUNCTION IF EXISTS ringing_time(varchar);
CREATE OR REPLACE FUNCTION ringing_time(start_date varchar) RETURNS TABLE(thetime timestamp without time zone, agent_id integer, total_ring_time integer)
AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'SELECT rtime, ragent_id, (CASE WHEN ring_time_answered IS NULL THEN ring_time_ignored
                                   WHEN ring_time_ignored IS NULL THEN ring_time_answered
                                   ELSE ring_time_answered + ring_time_ignored
                              END) AS total_ring_time FROM
        (SELECT round_to_15_minutes(CAST(stat.time AS  timestamp)) AS rtime, stat.agent_id AS ragent_id, CAST(sum(call.ring_duration_on_answer) AS INTEGER) AS ring_time_answered
            FROM stat_call_on_queue stat JOIN call_data call ON stat.callid = call.uniqueid
            WHERE stat.status = ''answered'' AND stat.time > CAST($1 AS timestamp)
            GROUP BY rtime, ragent_id) t1 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(CAST(q.time AS  timestamp)) AS rtime, s.id AS ragent_id, CAST(sum(CAST(q.data1 AS integer)/1000) AS INTEGER) AS ring_time_ignored
            FROM queue_log q INNER JOIN stat_agent s ON q.agent = s.name WHERE event = ''RINGNOANSWER'' AND CAST(time AS timestamp) > CAST($1 AS timestamp) 
            GROUP BY rtime, ragent_id) t2';
    RETURN QUERY
        EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;
