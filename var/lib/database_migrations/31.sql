SET ROLE asterisk;

CREATE OR REPLACE FUNCTION round_to_x_seconds(the_date timestamp without time zone, seconds integer) RETURNS timestamp without time zone AS
$$
BEGIN
    RETURN the_date::timestamp with time zone at time zone 'UTC' - (((CAST(extract(epoch FROM the_date) AS INTEGER) + 3*24*3600) % seconds) * INTERVAL '1 second');
END
$$
LANGUAGE plpgsql;
