SET ROLE asterisk;

ALTER TABLE call_on_queue ALTER COLUMN callid DROP NOT NULL;
