SET ROLE asterisk;

CREATE TABLE last_queue_log_id (
    id INTEGER
);

CREATE TABLE "call_on_queue" (
 "id" SERIAL PRIMARY KEY,
 "callid" VARCHAR(32) NOT NULL,
 "time" timestamp NOT NULL,
 "ringtime" INTEGER NOT NULL DEFAULT 0,
 "talktime" INTEGER NOT NULL DEFAULT 0,
 "waittime" INTEGER NOT NULL DEFAULT 0,
 "status" call_exit_type,
 "queue_ref" VARCHAR(128),
 "agent_num" VARCHAR(128)
);
CREATE INDEX "call_on_queue__idx__callid" ON call_on_queue(callid);

GRANT SELECT ON ALL TABLES IN SCHEMA public TO stats;

INSERT INTO call_on_queue(callid, time, ringtime, talktime, waittime, status, queue_ref, agent_num) (
    SELECT callid, time, ringtime, talktime, waittime, status, sq.name, regexp_replace(sa.name, 'Agent/', '')
    FROM stat_call_on_queue scq
    LEFT JOIN stat_agent sa ON scq.agent_id = sa.id
    LEFT JOIN stat_queue sq ON scq.queue_id = sq.id
);

DROP TABLE stat_call_on_queue;

INSERT INTO last_queue_log_id(id) (SELECT max(id) FROM queue_log);
