SET ROLE asterisk;

CREATE OR REPLACE FUNCTION insert_stat_queue_specific_with_cfg(start_date VARCHAR, end_date VARCHAR, time1 integer, time2 integer) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_queue_specific("time", queue_ref, dst_num, nb_offered, answer_less_t1, answer_btw_t1_t2, nb_abandoned, sum_resp_delay, abandoned_btw_t1_t2, abandoned_more_t2, communication_time, wrapup_time, hold_time) (
    SELECT thetime, queue_ref, dst_num, t1.nb_offered, t3.answer_less_t1, t3.answer_btw_t1_t2, t1.nb_abandoned, t1.sum_resp_delay, t1.abandoned_btw_t1_t2, t1.abandoned_more_t2, EXTRACT(epoch FROM t3.communication_time), t2.wrapup_time, EXTRACT(epoch FROM t4.hold_time) FROM
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num,
                sum(CASE WHEN cq.status IN (''answered'', ''abandoned'', ''leaveempty'', ''timeout'') OR cq.status IS NULL THEN 1 ELSE 0 END) AS nb_offered,
                sum(CASE WHEN cq.status = ''abandoned'' THEN 1 ELSE 0 END) AS nb_abandoned,
                sum(CASE WHEN cq.status = ''answered'' THEN EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) ELSE 0 END) AS sum_resp_delay,
                sum(CASE WHEN cq.status = ''abandoned'' AND EXTRACT(epoch FROM (cq.hangup_time - cq.queue_time)) > $3 AND EXTRACT(epoch FROM (cq.hangup_time - cq.queue_time)) <= $4 THEN 1 ELSE 0 END) AS abandoned_btw_t1_t2,
                sum(CASE WHEN cq.status = ''abandoned'' AND EXTRACT(epoch FROM (cq.hangup_time - cq.queue_time)) > $4 THEN 1 ELSE 0 END) AS abandoned_more_t2
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.queue_time >= $1::timestamp AND cq.queue_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t1 NATURAL FULL JOIN
        (SELECT $1::timestamp AS thetime, q.queuename AS queue_ref, c.dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid
            WHERE q.event = ''WRAPUPSTART'' AND q.time >= $1 AND q.time < $2
            GROUP BY thetime, queue_ref, c.dst_num) t2 NATURAL FULL JOIN
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num,
                sum(least($2::timestamp, cq.hangup_time) - greatest($1::timestamp, cq.answer_time)) AS communication_time,
                sum(CASE WHEN cq.answer_time >= $1::timestamp AND EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) <= $3 THEN 1 ELSE 0 END) AS answer_less_t1,
                sum(CASE WHEN cq.answer_time >= $1::timestamp AND EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) <= $4 AND EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) > $3 THEN 1 ELSE 0 END) AS answer_btw_t1_t2
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t3 NATURAL FULL JOIN
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num, sum(least($2::timestamp, hp.end) - greatest($1::timestamp, hp.start)) AS hold_time
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid INNER JOIN hold_periods hp ON hp.linkedid = c.uniqueid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL) AND hp.start < $2::timestamp AND (hp.end IS NULL OR hp.end >= $1::timestamp)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t4
    )';
    EXECUTE query USING start_date, end_date, time1, time2;
END
$$
LANGUAGE plpgsql;
