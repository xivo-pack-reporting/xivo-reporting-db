SET ROLE asterisk;

CREATE TEMPORARY TABLE tmp_call_on_queue AS
    SELECT callid, "time", ringtime, talktime, waittime, status, queue_ref, agent_num FROM call_on_queue;

DROP TABLE call_on_queue;
CREATE TABLE "call_on_queue" (
 "id" SERIAL PRIMARY KEY,
 "callid" VARCHAR(32) NOT NULL,
 "queue_time" timestamp NOT NULL,
 "total_ring_seconds" INTEGER NOT NULL DEFAULT 0,
 "answer_time" timestamp,
 "hangup_time" timestamp ,
 "status" call_exit_type,
 "queue_ref" VARCHAR(128),
 "agent_num" VARCHAR(128)
);

GRANT SELECT ON call_on_queue TO stats;

INSERT INTO call_on_queue (callid, queue_time, total_ring_seconds, answer_time, hangup_time, status, queue_ref, agent_num) (
    SELECT callid, "time", ringtime, CASE WHEN status = 'answered' THEN time + (INTERVAL '1 seconds' * waittime) ELSE NULL END, time + (INTERVAL '1 seconds' * (waittime + talktime)), status, queue_ref, agent_num FROM tmp_call_on_queue
);

CREATE INDEX "call_on_queue__idx__callid" ON call_on_queue(callid);
CREATE INDEX "call_on_queue__idx__queue_time" ON call_on_queue(queue_time);
CREATE INDEX "call_on_queue__idx__answer_time" ON call_on_queue(answer_time);

INSERT INTO call_data(uniqueid) (SELECT linkedid FROM pending_calls);
DROP TABLE pending_calls;

CREATE INDEX call_data__idx__end_time ON call_data(end_time);
CREATE INDEX call_data__idx__uniqueid ON call_data(uniqueid);
CREATE INDEX attached_data__idx__id_call_data ON attached_data(id_call_data);

