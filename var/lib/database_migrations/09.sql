SET ROLE asterisk;

CREATE OR REPLACE FUNCTION insert_stat_queue_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'INSERT INTO stat_queue_specific("time", queue_id, dst_num, nb_offered, nb_abandoned, sum_resp_delay, answer_less_15, abandoned_btw_15_20, answer_btw_15_20, abandoned_more_20, communication_time, wrapup_time) (
        SELECT t1.thetime, t1.queue_id, t1.dst_num, t1.nb_offered, t1.nb_abandoned, t1.sum_resp_delay, t1.answer_less_15, t1.abandoned_btw_15_20, t1.answer_btw_15_20, t1.abandoned_more_20, t1.communication_time, t2.wrapup_time FROM 
            (SELECT round_to_15_minutes(s.time) AS thetime, s.queue_id, c.dst_num,
                    sum(CASE WHEN s.status IN (''answered'', ''abandoned'', ''leaveempty'') THEN 1 ELSE 0 END) AS nb_offered,
                    sum(CASE WHEN s.status = ''abandoned'' THEN 1 ELSE 0 END) AS nb_abandoned,
                    sum(CASE WHEN s.status = ''answered'' THEN s.waittime ELSE 0 END) AS sum_resp_delay,
                    sum(CASE WHEN s.status = ''answered'' AND s.waittime <= 15 THEN 1 ELSE 0 END) AS answer_less_15,
                    sum(CASE WHEN s.status = ''abandoned'' AND s.waittime > 15 AND s.waittime <= 20 THEN 1 ELSE 0 END) AS abandoned_btw_15_20,
                    sum(CASE WHEN s.status = ''answered'' AND s.waittime <= 20 AND s.waittime > 15 THEN 1 ELSE 0 END) AS answer_btw_15_20,
                    sum(CASE WHEN s.status = ''abandoned'' AND s.waittime > 20 THEN 1 ELSE 0 END) AS abandoned_more_20,
                    sum(s.talktime) AS communication_time
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.time > CAST($1 AS timestamp)
                GROUP BY thetime, s.queue_id, c.dst_num) t1 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(c.start_time) AS thetime, c.dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
                FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid
                WHERE q.event = ''WRAPUPSTART'' AND c.start_time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t2
        )';
    EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;
