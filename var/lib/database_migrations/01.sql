SET ROLE asterisk;

ALTER TABLE stat_agent_specific RENAME COLUMN nb_internal_calls TO nb_received_internal_calls;
ALTER TABLE stat_agent_specific RENAME COLUMN internal_calls_comm_time TO conversation_time_internal_calls;

ALTER TABLE call_data ADD COLUMN ring_duration_on_answer INTEGER;
ALTER TABLE call_data ADD COLUMN transfered BOOLEAN;

-- fonction calculant le temps de sonnerie des agents (regroupés par quart d'heure)
DROP FUNCTION IF EXISTS ringing_time(varchar);
CREATE OR REPLACE FUNCTION ringing_time(start_date varchar) RETURNS TABLE(thetime timestamp without time zone, agent_id integer, total_ring_time integer)
AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'SELECT rtime, ragent_id, (CASE WHEN ring_time_answered IS NULL THEN ring_time_ignored
                                   WHEN ring_time_ignored IS NULL THEN ring_time_answered
                                   ELSE ring_time_answered + ring_time_ignored
                              END) AS total_ring_time FROM
        (SELECT round_to_15_minutes(CAST(stat.time AS  timestamp)) AS rtime, stat.agent_id AS ragent_id, CAST(sum(call.ring_duration_on_answer) AS INTEGER) AS ring_time_answered
            FROM stat_call_on_queue stat JOIN call_data call ON stat.callid = call.uniqueid
            WHERE stat.status = ''answered'' AND stat.time > CAST($1 AS timestamp)
            GROUP BY rtime, ragent_id) t1 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(CAST(q.time AS  timestamp)) AS rtime, s.id AS ragent_id, CAST(sum(CAST(q.data1 AS integer)/1000) AS INTEGER) AS ring_time_ignored
            FROM queue_log q INNER JOIN stat_agent s ON q.agent = s.name WHERE event = ''RINGNOANSWER'' AND CAST(time AS timestamp) > CAST($1 AS timestamp) 
            GROUP BY rtime, ragent_id) t2';
    RETURN QUERY
        EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS insert_stat_agent_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'INSERT INTO stat_agent_specific ("time", agent_id, nb_offered, nb_answered, conversation_time, ringing_time, nb_transfered, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_internal_calls) (
        SELECT thetime, agent_id, t1.nb_offered, t2.nb_answered, t3.conversation_time, t4.total_ring_time, t5.nb_transfered, t6.nb_outgoing_calls, t7.conversation_time_outgoing_calls, t8.nb_received_internal_calls, t9.conversation_time_internal_calls FROM
            (SELECT round_to_15_minutes(CAST(q.time AS  timestamp)) AS thetime, s.id AS agent_id, count(*) AS nb_offered
                FROM queue_log q INNER JOIN stat_agent s ON q.agent = s.name
                WHERE event IN(''CONNECT'', ''RINGNOANSWER'') AND CAST(time AS timestamp) > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t1 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(CAST(q.time AS  timestamp)) AS thetime, s.id AS agent_id, count(*) AS nb_answered
                FROM queue_log q INNER JOIN stat_agent s ON q.agent = s.name
                WHERE event = ''CONNECT'' AND CAST(time AS timestamp) > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t2 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(CAST(time AS  timestamp)) AS thetime, agent_id, sum(talktime) AS conversation_time
                FROM stat_call_on_queue WHERE status = ''answered'' AND time > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t3 NATURAL FULL JOIN
            (SELECT thetime, agent_id, total_ring_time FROM ringing_time($1)) t4 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(CAST(s.time AS  timestamp)) AS thetime, s.agent_id, count(*) AS nb_transfered
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND c.transfered is true AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, agent_id) t5 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c1.eventtime) AS thetime, a.agent_id, count(*) AS nb_outgoing_calls
                FROM cel c1 INNER JOIN cel c2 ON c1.uniqueid = c2.uniqueid INNER JOIN agent_position a ON a.line_number = c1.cid_num
                WHERE c1.eventtime > CAST($1 AS TIMESTAMP) AND c1.eventtype = ''CHAN_START'' AND c2.eventtype = ''APP_START'' AND c2.context = ''outcall''
                AND c1.eventtime >= a.start_time AND (c1.eventtime < a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t6 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c1.eventtime) AS thetime, a.agent_id, floor(EXTRACT(epoch FROM sum(c3.eventtime - c2.eventtime))) AS conversation_time_outgoing_calls
                FROM cel c1 INNER JOIN cel c2 ON c1.uniqueid = c2.uniqueid INNER JOIN cel c3 ON c2.uniqueid = c3.uniqueid INNER JOIN agent_position a ON a.line_number = c1.cid_num
                WHERE c1.eventtime > CAST($1 AS TIMESTAMP) AND c1.eventtype = ''CHAN_START'' AND c2.eventtype = ''ANSWER'' AND c2.context = ''outcall'' AND c3.eventtype = ''HANGUP''
                AND c1.eventtime >= a.start_time AND (c1.eventtime < a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t7 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_id, count(*) AS nb_received_internal_calls
                FROM call_data c INNER JOIN agent_position a ON a.line_number = c.dst_num
                WHERE c.end_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time <= a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t8 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_id, floor(EXTRACT(epoch FROM sum(c.end_time - c.answer_time))) AS conversation_time_internal_calls
                FROM call_data c INNER JOIN agent_position a ON a.line_number = c.dst_num
                WHERE c.end_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time <= a.end_time OR a.end_time IS NULL)
                GROUP BY thetime, agent_id) t9
    )';
    EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS insert_stat_agent_queue_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_queue_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'INSERT INTO stat_agent_queue_specific ("time", agent_id, queue_id, dst_num, nb_answered_calls, communication_time) (
        SELECT t1.thetime, t1.agent_id, t1.queue_id, t1.dst_num, t1.nb_answered_calls, t2.communication_time FROM
            (SELECT round_to_15_minutes(c.answer_time) AS thetime, s.agent_id AS agent_id, s.queue_id AS queue_id, c.dst_num AS dst_num, count(*) AS nb_answered_calls
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid WHERE s.status = ''answered'' AND c.answer_time >= CAST($1 AS timestamp)
                GROUP BY thetime, agent_id, queue_id, dst_num) t1 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.answer_time) AS thetime, s.agent_id AS agent_id, c.dst_num AS dst_num, sum(s.talktime) AS communication_time
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid WHERE s.status = ''answered'' AND c.answer_time >= CAST($1 AS timestamp)
                GROUP BY thetime, agent_id, dst_num) t2
    )';
    EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;

DROP TABLE IF EXISTS callid_infos;
DROP FUNCTION IF EXISTS insert_callid_infos(varchar);

ALTER TABLE stat_agent_queue_specific ADD COLUMN dst_num VARCHAR;

DROP FUNCTION IF EXISTS insert_stat_queue_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_queue_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'INSERT INTO stat_queue_specific("time", queue_id, dst_num, nb_offered, nb_abandoned, sum_resp_delay, answer_less_15, abandoned_btw_15_20, answer_btw_15_20, abandoned_more_20, communication_time) (
        SELECT t1.thetime, t1.queue_id, t1.dst_num, t1.nb_offered, t2.nb_abandoned, t3.sum_resp_delay, t4.answer_less_15, t5.abandoned_btw_15_20, t6.answer_btw_15_20, t7.abandoned_more_20, t8.communication_time FROM 
            (SELECT round_to_15_minutes(s.time) AS thetime, s.queue_id, c.dst_num, count(*) AS nb_offered
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status IN(''answered'', ''abandoned'') AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, s.queue_id, c.dst_num) t1 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS nb_abandoned
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''abandoned'' AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t2 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, sum(s.waittime) AS sum_resp_delay
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t3 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS answer_less_15
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND s.waittime <= 15 AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t4 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS abandoned_btw_15_20
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''abandoned'' AND s.waittime > 15 AND s.waittime <= 20 AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t5 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS answer_btw_15_20
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''answered'' AND s.waittime <= 20 AND s.waittime > 15 AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t6 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, count(*) AS abandoned_more_20
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.status = ''abandoned'' AND s.waittime > 20 AND s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t7 NATURAL LEFT JOIN
            (SELECT round_to_15_minutes(s.time) AS thetime, c.dst_num, sum(talktime) AS communication_time
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid
                WHERE s.time > CAST($1 AS timestamp)
                GROUP BY thetime, c.dst_num) t8 
        )';
    EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;

