SET ROLE asterisk;

DROP FUNCTION IF EXISTS insert_stat_agent_queue_specific(varchar, varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_queue_specific(start_date VARCHAR, end_date varchar) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_agent_queue_specific ("time", agent_num, queue_ref, dst_num, nb_answered_calls, communication_time, wrapup_time, hold_time) (
    SELECT $1::timestamp, t1.agent_num, t1.queue_ref, t1.dst_num, t1.nb_answered_calls, EXTRACT(epoch FROM t1.communication_time), t2.wrapup_time, EXTRACT(epoch FROM t3.hold_time) FROM
        (SELECT cq.agent_num AS agent_num, cq.queue_ref AS queue_ref, c.dst_num AS dst_num,
                count(*) AS nb_answered_calls,
                sum(least($2::timestamp, cq.hangup_time) - greatest($1::timestamp, cq.answer_time)) AS communication_time
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.status = ''answered'' AND cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY agent_num, queue_ref, dst_num) t1 NATURAL FULL JOIN
        (SELECT regexp_replace(q.agent, ''Agent/'', '''') AS agent_num, q.queuename AS queue_ref, c.dst_num AS dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid WHERE event = ''WRAPUPSTART'' AND time >= $1 AND time < $2
            GROUP BY agent_num, queue_ref, dst_num) t2 NATURAL FULL JOIN
        (SELECT cq.agent_num, cq.queue_ref, cd.dst_num, sum(least($2::timestamp, hp.end) - greatest($1::timestamp, hp.start)) AS hold_time
            FROM call_on_queue cq INNER JOIN call_data cd ON cq.callid = cd.uniqueid INNER JOIN hold_periods hp ON hp.linkedid = cq.callid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL) AND hp.start < $2::timestamp AND (hp.end IS NULL OR hp.end >= $1::timestamp)
            GROUP BY cq.agent_num, queue_ref, dst_num) t3
    )';
    EXECUTE query USING start_date, end_date;
END
$$
LANGUAGE plpgsql;
