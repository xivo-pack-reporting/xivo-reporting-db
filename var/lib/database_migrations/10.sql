SET ROLE asterisk;

CREATE OR REPLACE FUNCTION insert_stat_agent_queue_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    request VARCHAR;
BEGIN
    request := 'INSERT INTO stat_agent_queue_specific ("time", agent_id, queue_id, dst_num, nb_answered_calls, communication_time, wrapup_time) (
        SELECT t1.thetime, t1.agent_id, t1.queue_id, t1.dst_num, t1.nb_answered_calls, t1.communication_time, t2.wrapup_time FROM
            (SELECT round_to_15_minutes(c.start_time) AS thetime, s.agent_id AS agent_id, s.queue_id AS queue_id, c.dst_num AS dst_num, count(*) AS nb_answered_calls, sum(s.talktime) AS communication_time
                FROM stat_call_on_queue s INNER JOIN call_data c ON s.callid = c.uniqueid WHERE s.status = ''answered'' AND c.start_time >= CAST($1 AS timestamp)
                GROUP BY thetime, agent_id, queue_id, dst_num) t1 NATURAL FULL JOIN
            (SELECT round_to_15_minutes(c.start_time) AS thetime, s.agent_id AS agent_id, c.dst_num AS dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
                FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid JOIN stat_call_on_queue s ON s.callid = q.callid
                WHERE q.event = ''WRAPUPSTART'' AND c.start_time >= CAST($1 AS timestamp)
                GROUP BY thetime, agent_id, dst_num) t2
    )';
    EXECUTE request USING start_date;
END;
$$
LANGUAGE plpgsql;
