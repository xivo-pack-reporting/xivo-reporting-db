SET ROLE asterisk;

CREATE TEMPORARY TABLE copy_stat_queue_periodic AS
    SELECT time, sq.name AS queue, answered, abandoned, total, "full", closed, joinempty, leaveempty, divert_ca_ratio, divert_waittime, timeout
    FROM stat_queue_periodic sqp JOIN stat_queue sq ON sqp.queue_id = sq.id;

DROP TABLE IF EXISTS "stat_queue_periodic" CASCADE;
CREATE TABLE "stat_queue_periodic" (
 "id" SERIAL PRIMARY KEY,
 "time" timestamp NOT NULL,
 "queue" VARCHAR(128) NOT NULL,
 "answered" INTEGER NOT NULL DEFAULT 0,
 "abandoned" INTEGER NOT NULL DEFAULT 0,
 "total" INTEGER NOT NULL DEFAULT 0,
 "full" INTEGER NOT NULL DEFAULT 0,
 "closed" INTEGER NOT NULL DEFAULT 0,
 "joinempty" INTEGER NOT NULL DEFAULT 0,
 "leaveempty" INTEGER NOT NULL DEFAULT 0,
 "divert_ca_ratio" INTEGER NOT NULL DEFAULT 0,
 "divert_waittime" INTEGER NOT NULL DEFAULT 0,
 "timeout" INTEGER NOT NULL DEFAULT 0
);

DROP FUNCTION IF EXISTS round_to_x_seconds(timestamp without time zone, integer);
CREATE OR REPLACE FUNCTION round_to_x_seconds(the_date timestamp without time zone, seconds integer) RETURNS timestamp without time zone AS
$$
BEGIN
  RETURN the_date - ((CAST(extract(epoch FROM the_date) AS INTEGER) % seconds) * INTERVAL '1 second');
END
$$
LANGUAGE plpgsql;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO stats;

INSERT INTO stat_queue_periodic(time, queue, answered, abandoned, total, "full", closed, joinempty, leaveempty, divert_ca_ratio, divert_waittime, timeout) (
    SELECT time, queue, answered, abandoned, total, "full", closed, joinempty, leaveempty, divert_ca_ratio, divert_waittime, timeout FROM copy_stat_queue_periodic);

