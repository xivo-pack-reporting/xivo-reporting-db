SET ROLE asterisk;

DROP FUNCTION IF EXISTS insert_stat_agent_specific(VARCHAR);
CREATE OR REPLACE FUNCTION insert_stat_agent_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_agent_specific ("time", agent_num, nb_offered, nb_answered, conversation_time, ringing_time, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_received_internal_calls, nb_transfered_intern, nb_transfered_extern, nb_emitted_internal_calls, conversation_time_emitted_internal_calls, nb_incoming_calls, conversation_time_incoming_calls) (
    SELECT thetime, agent_num, t1.nb_offered, t2.nb_answered, t2.conversation_time, t3.total_ring_time, t4.nb_outgoing_calls, t4.conversation_time_outgoing_calls, t5.nb_received_internal_calls, t5.conversation_time_received_internal_calls, t2.nb_transfered_intern, t2.nb_transfered_extern, t4.nb_emitted_internal_calls, t4.conversation_time_emitted_internal_calls, t6.nb_incoming_calls, t6.conversation_time_incoming_calls FROM
        (SELECT round_to_15_minutes(c.end_time) AS thetime, regexp_replace(q.agent, ''Agent/'', '''') AS agent_num, count(*) AS nb_offered
            FROM queue_log q INNER JOIN call_data c ON c.uniqueid = q.callid
            WHERE event IN(''CONNECT'', ''RINGNOANSWER'') AND c.end_time > CAST($1 AS timestamp)
            GROUP BY thetime, agent_num) t1 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, cq.agent_num,
                count(*) AS nb_answered,
                sum(talktime) AS conversation_time,
                sum(CASE WHEN c.transfered IS TRUE AND c.transfer_direction = ''internal'' THEN 1 ELSE 0 END) AS nb_transfered_intern,
                sum(CASE WHEN c.transfered IS TRUE AND c.transfer_direction = ''outgoing'' THEN 1 ELSE 0 END) AS nb_transfered_extern
            FROM call_on_queue cq JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.status = ''answered'' AND c.end_time > CAST($1 AS timestamp)
            GROUP BY thetime, agent_num) t2 NATURAL FULL JOIN
        (SELECT thetime, agent_num, total_ring_time FROM ringing_time($1)) t3 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_num,
                sum(CASE WHEN c.call_direction = ''outgoing'' THEN 1 ELSE 0 END) AS nb_outgoing_calls,
                floor(EXTRACT(epoch FROM sum(CASE WHEN c.call_direction = ''outgoing'' THEN c.end_time - c.answer_time ELSE INTERVAL ''0'' END))) AS conversation_time_outgoing_calls,
                sum(CASE WHEN c.call_direction = ''internal'' THEN 1 ELSE 0 END) AS nb_emitted_internal_calls,
                floor(EXTRACT(epoch FROM sum(CASE WHEN c.call_direction = ''internal'' THEN c.end_time - c.answer_time ELSE INTERVAL ''0'' END))) AS conversation_time_emitted_internal_calls
            FROM call_data c INNER JOIN agent_position a ON a.line_number = c.src_num
            WHERE c.end_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
            GROUP BY thetime, agent_num) t4 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_num, count(*) AS nb_received_internal_calls, floor(EXTRACT(epoch FROM sum(c.end_time - c.answer_time))) AS conversation_time_received_internal_calls
            FROM call_data c INNER JOIN agent_position a ON a.line_number = c.dst_num
            WHERE c.call_direction = ''internal'' AND c.end_time > CAST($1 AS TIMESTAMP) AND c.start_time >= a.start_time AND (c.start_time <= a.end_time OR a.end_time IS NULL)
            GROUP BY thetime, agent_num) t5 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, a.agent_num, count(*) AS nb_incoming_calls, floor(EXTRACT(epoch FROM sum(c.end_time - c.answer_time))) AS conversation_time_incoming_calls
            FROM call_data c INNER JOIN agent_position a ON a.sda = c.dst_num
            WHERE c.end_time > CAST($1 AS TIMESTAMP) AND c.call_direction = ''incoming''
            AND c.start_time >= a.start_time AND (c.start_time < a.end_time OR a.end_time IS NULL)
            GROUP BY thetime, agent_num) t6
)';
    EXECUTE query USING start_date;
END
$$
LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS ringing_time(VARCHAR);
CREATE OR REPLACE FUNCTION ringing_time(start_date varchar) RETURNS TABLE(thetime timestamp without time zone, agent_num varchar, total_ring_time integer)
AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'SELECT rtime, ragent_num, (CASE WHEN ring_time_answered IS NULL THEN ring_time_ignored
                               WHEN ring_time_ignored IS NULL THEN ring_time_answered
                               ELSE ring_time_answered + ring_time_ignored
                          END) AS total_ring_time FROM
    (SELECT round_to_15_minutes(call.end_time) AS rtime, cq.agent_num AS ragent_num, CAST(sum(call.ring_duration_on_answer) AS INTEGER) AS ring_time_answered
        FROM call_on_queue cq JOIN call_data call ON cq.callid = call.uniqueid
        WHERE cq.status = ''answered'' AND call.end_time > CAST($1 AS timestamp)
        GROUP BY rtime, ragent_num) t1 NATURAL FULL JOIN
    (SELECT round_to_15_minutes(c.end_time) AS rtime, regexp_replace(q.agent, ''Agent/'', '''') AS ragent_num, CAST(sum(CAST(q.data1 AS integer)/1000) AS INTEGER) AS ring_time_ignored
        FROM queue_log q INNER JOIN call_data c ON c.uniqueid = q.callid
        WHERE event = ''RINGNOANSWER'' AND c.end_time > CAST($1 AS timestamp) 
        GROUP BY rtime, ragent_num) t2';
    RETURN QUERY
         EXECUTE query USING start_date;
END
$$
LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS insert_stat_agent_queue_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_queue_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_agent_queue_specific ("time", agent_num, queue_ref, dst_num, nb_answered_calls, communication_time, wrapup_time) (
    SELECT t1.thetime, t1.agent_num, t1.queue_ref, t1.dst_num, t1.nb_answered_calls, t1.communication_time, t2.wrapup_time FROM
        (SELECT round_to_15_minutes(c.end_time) AS thetime, cq.agent_num AS agent_num, cq.queue_ref AS queue_ref, c.dst_num AS dst_num, count(*) AS nb_answered_calls, sum(cq.talktime) AS communication_time
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid WHERE cq.status = ''answered'' AND c.end_time >= CAST($1 AS timestamp)
            GROUP BY thetime, agent_num, queue_ref, dst_num) t1 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, regexp_replace(q.agent, ''Agent/'', '''') AS agent_num, q.queuename AS queue_ref, c.dst_num AS dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid
            WHERE q.event = ''WRAPUPSTART'' AND c.end_time >= CAST($1 AS timestamp)
            GROUP BY thetime, agent_num, queue_ref, dst_num) t2
)';
    EXECUTE query USING start_date;
END
$$
LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS insert_stat_queue_specific(varchar);
CREATE OR REPLACE FUNCTION insert_stat_queue_specific(start_date VARCHAR) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_queue_specific("time", queue_ref, dst_num, nb_offered, answer_less_15, answer_btw_15_20, nb_abandoned, sum_resp_delay, abandoned_btw_15_20, abandoned_more_20, communication_time, wrapup_time) (
    SELECT thetime, queue_ref, dst_num, t1.nb_offered, t1.answer_less_15, t1.answer_btw_15_20, t1.nb_abandoned, t1.sum_resp_delay, t1.abandoned_btw_15_20, t1.abandoned_more_20, t1.communication_time, t2.wrapup_time FROM 
        (SELECT round_to_15_minutes(c.end_time) AS thetime, cq.queue_ref, c.dst_num,
                sum(CASE WHEN cq.status IN (''answered'', ''abandoned'', ''leaveempty'') THEN 1 ELSE 0 END) AS nb_offered,
                sum(CASE WHEN cq.status = ''answered'' AND cq.waittime <= 15 THEN 1 ELSE 0 END) AS answer_less_15,
                sum(CASE WHEN cq.status = ''answered'' AND cq.waittime <= 20 AND cq.waittime > 15 THEN 1 ELSE 0 END) AS answer_btw_15_20,
                sum(CASE WHEN cq.status = ''abandoned'' THEN 1 ELSE 0 END) AS nb_abandoned,
                sum(CASE WHEN cq.status = ''answered'' THEN cq.waittime ELSE 0 END) AS sum_resp_delay,
                sum(CASE WHEN cq.status = ''abandoned'' AND cq.waittime > 15 AND cq.waittime <= 20 THEN 1 ELSE 0 END) AS abandoned_btw_15_20,
                sum(CASE WHEN cq.status = ''abandoned'' AND cq.waittime > 20 THEN 1 ELSE 0 END) AS abandoned_more_20,
                sum(cq.talktime) AS communication_time
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE c.end_time > CAST($1 AS timestamp)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t1 NATURAL FULL JOIN
        (SELECT round_to_15_minutes(c.end_time) AS thetime, q.queuename AS queue_ref, c.dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid
            WHERE q.event = ''WRAPUPSTART'' AND c.end_time > CAST($1 AS timestamp)
            GROUP BY thetime, queue_ref, c.dst_num) t2
    )';
    EXECUTE query USING start_date;
END
$$
LANGUAGE plpgsql;

