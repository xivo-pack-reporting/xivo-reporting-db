SET ROLE asterisk;

CREATE INDEX "stat_agent_periodic__idx__time" ON stat_agent_periodic(time);
CREATE INDEX "stat_queue_periodic__idx__time" ON stat_queue_periodic(time);
CREATE INDEX "stat_queue_specific__idx__time" ON stat_queue_specific(time);
CREATE INDEX "stat_agent_specific__idx__time" ON stat_agent_specific(time);
CREATE INDEX "stat_agent_queue_specific__idx__time" ON stat_agent_queue_specific(time);
