POSTGRESQL_VERSION=`ls /etc/postgresql`
echo "Augmentation de la mémoire allouée à postgres"
echo "work_mem = 4MB" >> /etc/postgresql/$POSTGRESQL_VERSION/main/postgresql.conf
/etc/init.d/postgresql restart
