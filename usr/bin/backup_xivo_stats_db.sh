#!/bin/bash

DATE=$(date +%Y%m%d-%H%M%S)
FILEBASENAME=${DATE}-db-xivo-stats
DUMPFILE=${FILEBASENAME}.dump
ARCHIVEFILE=${FILEBASENAME}.tgz
DESTDIR=/var/backups/xivo_stats_db

if [ "$(whoami)" != "root" ]; then
    echo "This script needs to be executed with sudo (as root)."
    exit 1
fi


echo Dumping xivo-stats database to ${FILEBASENAME}.tgz

PWD=$(pwd)

if [ ! -d $DESTDIR ]; then
    mkdir $DESTDIR
fi

cd $DESTDIR 
sudo -u postgres pg_dump -Fc xivo_stats > /tmp/${DUMPFILE}
tar -czf ${ARCHIVEFILE} -C / tmp/${DUMPFILE}
rm /tmp/${DUMPFILE}

cd $PWD

